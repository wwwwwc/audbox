众里寻他千百度，蓦然回首，那人却在，灯火阑珊处。——辛弃疾《青玉案·元夕》
月上柳梢头，人约黄昏后。——欧阳修《生查子·元夕》
东风夜放花千树，更吹落、星如雨。——辛弃疾《青玉案·元夕》
去年元夜时，花市灯如昼。——欧阳修《生查子·元夕》
火树银花合，星桥铁锁开。——苏味道《正月十五夜》
谁教岁岁红莲夜，两处沉吟各自知。——姜夔《鹧鸪天·元夕有所梦》
火冷灯稀霜露下，昏昏雪意云垂野。——苏轼《蝶恋花·密州上元》
凤箫声动，玉壶光转，一夜鱼龙舞。——辛弃疾《青玉案·元夕》
凤凰城阙知何处，寥落星河一雁飞。——贺铸《思越人·紫府东风放夜时》
宝马雕车香满路。——辛弃疾《青玉案·元夕》
灯火钱塘三五夜，明月如霜，照见人如画。——苏轼《蝶恋花·密州上元》
不见去年人，泪湿春衫袖。——欧阳修《生查子·元夕》
暗尘随马去，明月逐人来。——苏味道《正月十五夜》
今年元夜时，月与灯依旧。——欧阳修《生查子·元夕》
那里有闹红尘香车宝马？祗不过送黄昏古木寒鸦。——王磐《古蟾宫·元宵》
而今灯漫挂。不是暗尘明月，那时元夜。——蒋捷《女冠子·元夕》
五更钟动笙歌散，十里月明灯火稀。——贺铸《思越人·紫府东风放夜时》
蛾儿雪柳黄金缕，笑语盈盈暗香去。——辛弃疾《青玉案·元夕》
千门开锁万灯明，正月中旬动帝京。——张祜《正月十五夜灯》
金吾不禁夜，玉漏莫相催。——苏味道《正月十五夜》
听元宵，今岁嗟呀，愁也千家，怨也千家。——王磐《古蟾宫·元宵》
帐底吹笙香吐麝，更无一点尘随马。——苏轼《蝶恋花·密州上元》
来相召、香车宝马，谢他酒朋诗侣。——李清照《永遇乐·落日熔金》
有灯无月不娱人，有月无灯不算春。——唐寅《元宵》
花满市，月侵衣。——姜夔《鹧鸪天·正月十一日观灯》
元宵佳节，融和天气，次第岂无风雨。——李清照《永遇乐·落日熔金》
接汉疑星落，依楼似月悬。——卢照邻《十五夜观灯》
故园今夕是元宵，独向蛮村坐寂寥。——王守仁《元夕二首》
袨服华妆着处逢，六街灯火闹儿童。——元好问《京都元夕》
听元宵，往岁喧哗，歌也千家，舞也千家。——王磐《古蟾宫·元宵》
锦里开芳宴，兰缸艳早年。——卢照邻《十五夜观灯》
月色灯山满帝都，香车宝盖隘通衢。——李商隐《观灯乐行》
玉皇开碧落，银界失黄昏。——毛滂《临江仙·都城元夕》
三百内人连袖舞，一时天上著词声。——张祜《正月十五夜灯》
美人慵翦上元灯，弹泪倚瑶瑟。——朱敦儒《好事近·春雨细如尘》
闻道长安灯夜好，雕轮宝马如云。——毛滂《临江仙·都城元夕》
不是暗尘明月，那时元夜。——蒋捷《女冠子·元夕》
谁家见月能闲坐？何处闻灯不看来？——崔液《上元夜六首·其一》
铺翠冠儿，撚金雪柳，簇带争济楚。——李清照《永遇乐·落日熔金》
玉漏银壶且莫催，铁关金锁彻明开。——崔液《上元夜六首·其一》
箫鼓喧，人影参差，满路飘香麝。——周邦彦《解语花·上元》
不展芳尊开口笑，如何消得此良辰。——唐寅《元宵》
别有千金笑，来映九枝前。——卢照邻《十五夜观灯》
长衫我亦何为者，也在游人笑语中。——元好问《京都元夕》
望千门如昼，嬉笑游冶。——周邦彦《解语花·上元》
见说马家滴粉好，试灯风里卖元宵。——符曾《上元竹枝词》
桂花香馅裹胡桃，江米如珠井水淘。——符曾《上元竹枝词》
宣和旧日，临安南渡，芳景犹自如故。——刘辰翁《永遇乐·璧月初晴》
中州盛日，闺门多暇，记得偏重三五。——李清照《永遇乐·落日熔金》
风消焰蜡，露浥红莲，花市光相射。——周邦彦《解语花·风销焰蜡》
月傍苑楼灯影暗，风传阁道马蹄回。——王守仁《元夕二首》
相逢处，自有暗尘随马。——周邦彦《解语花·上元》
游伎皆秾李，行歌尽落梅。——苏味道《正月十五夜》
紫府东风放夜时。——贺铸《思越人·紫府东风放夜时》
霭芳阴未解，乍天气、过元宵。——周端臣《木兰花慢·送人之官九华》
九衢雪小，千门月淡，元宵灯近。——晁端礼《水龙吟·咏月》
缛彩遥分地，繁光远缀天。——卢照邻《十五夜观灯》
春风飞到，宝钗楼上，一片笙箫，琉璃光射。——蒋捷《女冠子·元夕》
击鼓吹箫，却入农桑社。——苏轼《蝶恋花·密州上元》
将坛醇酒冰浆细，元夜邀宾灯火新。——赵时春《元宵饮陶总戎家二首》
身闲不睹中兴盛，羞逐乡人赛紫姑。——李商隐《正月十五夜闻京有灯恨不得观》
去年今日卧燕台，铜鼓中宵隐地雷。——王守仁《元夕二首》
天涯寒尽减春衣。——贺铸《思越人·紫府东风放夜时》
步莲秾李伴人归。——贺铸《思越人·紫府东风放夜时》
清漏移，飞盖归来，从舞休歌罢。——周邦彦《解语花·上元》